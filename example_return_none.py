from typing import NoReturn


def play(player_name: str) -> None:
    print(f"player name: {player_name}")


def black_hole() -> NoReturn:
    raise Exception("There is no going back...")


print(play("Maria"))